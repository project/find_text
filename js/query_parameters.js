(function (Drupal, once) {
  const find_text_params = {};
  Drupal.behaviors.findTextParameters = {

    attach: function (context) {
      find_text_params.form = once('find-text-parameters', document.querySelector('[data-drupal-selector="find-text"]'));
      if (find_text_params.form.length > 0) {
        find_text_params.form.forEach(init);
      }
    }
  };

  function init(value, index) {
    find_text_params.text = value.querySelector('[data-drupal-selector="edit-needle"]');
    find_text_params.markup = value.querySelector('[data-drupal-selector="edit-render"]');
    find_text_params.regex = value.querySelector('[data-drupal-selector="edit-regexed"]');

    defineChange(find_text_params.text, textChange, 'text', 'input');
    defineChange(find_text_params.markup, checkboxChange, 'markup');
    defineChange(find_text_params.regex, checkboxChange, 'regex');
  }

  function textChange(event, element) {
    const valid = element.value.length >= 3;
    const url = new URL(location);

    if(valid) {
      url.searchParams.set('text', encodeURI(element.value));
    }
    else {
      url.searchParams.delete('text');
    }

    history.replaceState({}, "", url);
  }

  function checkboxChange(event, element, id) {
    const url = new URL(location);
    url.searchParams.set(id, element.checked ? 'true' : 'false');
    history.replaceState({}, "", url);
  }

  function defineChange(element, callback, id, eventType='change') {
    element.addEventListener(eventType, (event) => {
      callback(event, element, id);
    });
  }
}(Drupal, once));
