<?php

namespace Drupal\find_text\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Configure Search settings for this site.
 */
class SearchForm extends ConfigFormBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * For Managing cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheManager;

  /**
   * Cache Tags Invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): mixed {
    $instance = parent::create($container);
    $instance->renderer = $container->get('renderer');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->cacheManager = $container->get('cache.data');
    $instance->cacheTagsInvalidator = $container->get('cache_tags.invalidator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'find_text';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $raw_query_string = \Drupal::request();
    $raw_query_search_text = $raw_query_string->get('text');
    $raw_query_render_as_markup = $raw_query_string->get('markup');
    $raw_query_regex = $raw_query_string->get('regex');

    $query_search_text = $raw_query_search_text ? urldecode($raw_query_search_text) : NULL;
    $query_render_as_markup = $raw_query_render_as_markup ? $raw_query_render_as_markup === 'true' : NULL;
    $query_regex = $raw_query_regex ? $raw_query_regex === 'true' : NULL;

    $wrapper_id = $this->getFormId() . '-wrapper';

    // Ajax seems to work best when we put all our logic in the buldForm method.
    // Load form_state values.
    $results = $form_state->get('results') ?? FALSE;
    $has_results = $results ? TRUE : FALSE;

    // Going to need to handle the results generation and rendering differently to work with CSV generation.
    $results_table = $form_state->get('results_table') ?? FALSE;
    $results_markup = $form_state->get('results_markup') ?? '';

    


    $form = [
      '#prefix' => '<div id="' . $wrapper_id . '" aria-live="polite">',
      '#suffix' => '</div>',
    ];
    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => <<< 'EOD'
        <p>Search text fields for a provided string. The search is not case-sensitive.</p>
        <p>Pre-rendered text area markup is searched, so some characters may be different; for instance, `&amp ;` may match ampersands where `&` will not.
        Node fields and content blocks are included, but some areas such as menu links will not be searched.</p>
      EOD,
    ];
    $form['needle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search Text'),
      '#default_value' => $query_search_text ?: '',
      '#description' => $this->t('The string to search against. Wildcards % will match any number of characters and _ will match any single character. % wildcards are prepended and appended automatically when not using regex. See the <a href="https://sitenow.uiowa.edu/documentation/site-text-search">Find Text documentation</a> for more information.'),
    ];
    $form['render'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Render markup'),
      '#description' => $this->t('Display the results as rendered HTML. This may hide parts of the results, such as text matched within HTML tags.'),
      '#default_value' => $query_render_as_markup ?? 1,
    ];
    $form['regexed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('REGEXP?'),
      '#description' => $this->t('% wildcards will not be prepended or appended, and the search will be performed as a REGEXP search rather than LIKE. Use only if a full regular expression is required.'),
      '#default_value' => $query_regex ?? 0,
    ];
    // Show invalidate cache checkbox only if search results caching is enabled.
    if ($this->configFactory()->get('find_text.settings')->get('enable_search_results_cache')) {
      $form['invalidate_cache'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Invalidate cache'),
        '#description' => $this->t('If checked, the cache for search results will be cleared and the search will be performed again.'),
        '#default_value' => 0,
      ];
    }
    // Check if languages or localization is enabled
    // and create an options list of the configured languages.
    $language_options = [
      'All' => $this->t('All'),
    ];
    if ($localization_enabled = \Drupal::moduleHandler()->moduleExists('language')) {
      $languages = \Drupal::languageManager()->getLanguages();
      foreach ($languages as $langcode => $language) {
        $language_options[$langcode] = $language->getName();
      }
    }
    $form['language_selection'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => $language_options,
      '#default_value' => 'All',
      '#access' => $localization_enabled,
    ];

    // Check if workflows module is enabled
    // and create an options list of the configured workflows.
    if (\Drupal::moduleHandler()->moduleExists('workflows')) {
      $workflows = \Drupal::entityTypeManager()->getStorage('workflow')->loadMultiple();
      $workflow_options = ['All' => $this->t('All')];
      foreach ($workflows as $workflow) {
        // Get all of the states for the workflow
        // and populate the options list with them.
        $states = $workflow->getTypePlugin()->getStates();
        foreach ($states as $state) {
          $workflow_options[$state->id()] = $state->label();
        }
      }
      // Add a select box for selecting a workflow.
      $form['workflow_selection'] = [
        '#type' => 'select',
        '#title' => $this->t('Workflow'),
        '#options' => $workflow_options,
        '#default_value' => 'All',
      ];
    }
    else {
      // If workflows module is not enabled,
      // add a filter for published/unpublished nodes.
      $form['published_state'] = [
        '#type' => 'select',
        '#title' => $this->t('Status'),
        '#options' => [
          'All' => $this->t('All'),
          'Published' => $this->t('Published'),
          'Unpublished' => $this->t('Unpublished'),
        ],
        '#default_value' => 'All',
      ];
    }

    // Add an option to filter by content type.
    $node_types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    $node_type_options = ['All' => $this->t('All')];
    foreach ($node_types as $node_type) {
      $node_type_options[$node_type->id()] = $node_type->label();
    }
    $form['node_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content Type'),
      '#options' => $node_type_options,
      '#default_value' => 'All',
    ];

    $form['search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#button_type' => 'primary',
      '#name' => 'search',
      '#submit' => ['::generateResults'],
      '#ajax' => [
        'callback' => '::findTextAjaxCallback',
        'wrapper' => $wrapper_id,
        'method' => 'replaceWith',
        'disable-refocus' => TRUE,
        'effect' => 'fade',
        'event' => 'click',
      ],
    ];
    $form['generate_csv'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download CSV'),
      '#name' => 'generate_csv',
      '#submit' => ['::generateCsv'],
      '#disabled' => !$has_results,
      '#access' => $this->configFactory()->get('find_text.settings')->get('save_as_csv') === 1,
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#name' => 'reset',
      '#submit' => [
        [$this, 'resetButton'],
      ],
    ];

    $form['results'] = [
      '#type' => 'markup',
      '#markup' => $results_markup,
    ];

    // Unset the original, currently unused submit button.
    // It might be used at another time if settings are needed.
    unset($form['actions']['submit']);
    // Attach our custom css for highlighting purposes.
    $form['#attached']['library'][] = 'find_text/results';
    return $form;
  }

  /**
   * Return rebuilt form with ajax.
   */
  public function findTextAjaxCallback(array &$form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * Generate a CSV file from search results.
   */
  public function generateCsv(array &$form, FormStateInterface $form_state): void {

    $results = $form_state->get('filtered_results');

      $response = $this->generateCsvFile($results);
      $form_state->setResponse($response);

    $form_state->setRebuild();
  }


  /**
   * Perform a search with the given needle.
   */
  public function generateResults(array &$form, FormStateInterface $form_state) {
    $needle = $form_state->getValue('needle');
    // Only handle the processing if we have
    // a needle length of 3 or more characters.
    if (strlen($needle) >= 3) {
      $regexed = $form_state->getValue('regexed');
      $render = $form_state->getValue('render');
      $langcode = $form_state->getValue('language_selection');
      // Clear the cache for search results if invalidate cache option
      // is selected.
      if ($form_state->getValue('invalidate_cache')) {
        $this->cacheTagsInvalidator->invalidateTags(['search_results:data']);
      }
      $find_text_settings = $this->configFactory()->get('find_text.settings');
      $is_cache_enabled = $find_text_settings->get('enable_search_results_cache');
      $cid = 'search_results:' . md5($needle);
      // Check if search results caching is enabled and data is
      // available in the cache.
      if ($is_cache_enabled && $cache = $this->cacheManager->get($cid)) {
        $results = $cache->data['results'];
        $filtered_results = $cache->data['table'];
        $renderable_results = $cache->data['renderable_results'];
      }
      else {
        $results = search_fields($needle, $regexed, $render, $langcode);
        $status_filter = $form_state->getValue('published_state') ?? $form_state->getValue('workflow_selection');
        $bundle_filter = $form_state->getValue('node_type');
        $filtered_results = $this->filterResultsTable($results, $status_filter, $bundle_filter);
        $renderable_results = $this->renderResultsTable($filtered_results);
        

        // Store the data in cache if search results caching is enabled.
        if ($is_cache_enabled) {
          $cache_expiry = time() + $find_text_settings->get('search_results_cache_duration') ?? 3600;
          // Need to store results and table both as we need results array
          // to generate the search results CSV.
          $this->cacheManager->set(
            'search_results:' . md5($needle),
            [
              'results' => $results,
              'table' => $filtered_results,
              'renderable_results' => $renderable_results,
            ],
            $cache_expiry,
            ['search_results:data']
          );
        }
      }

      $rendered_results = $this->renderer->render($renderable_results);

      $form_state->set('results', $results);
      $form_state->set('filtered_results', $filtered_results);
      $form_state->set('results_markup', $rendered_results);



      $form['results'] = [
        '#type' => 'markup',
        '#markup' => $rendered_results,
      ];

    }
    $form_state->setRebuild(TRUE);

  }

  /**
   * Helper function to generate a CSV file from search results.
   *
   * @param array $results
   *   The search form results to export.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The binary response containing the file.
   */
  public function generateCsvFile($results): BinaryFileResponse {

    $file_directory = $this->configFactory()->get('system.file')->get('default_scheme') . '://';
    $file_name = 'search_results_' . date('Y-m-d_H-i-s') . '.csv';
    $csv_file_path = $file_directory . $file_name;
    $csv_file = fopen($csv_file_path, 'w');

    $header = $results['#header'];
    $header = [
      'id' => 'Entity ID',
      'entity_type' => 'Entity Type',
      'title' => 'Title',
      'content type' => 'Content Type',
      // 'node_url' => 'Node URL',
      // 'edit_url' => 'Edit URL',
      // 'lb_edit_url' => 'Layout Builder URL',
      'status' => 'Status',
      'author' => 'Author',
      'created' => 'Created',
      'updated' => 'Updated',
      'field' => 'Field: Contents',
    ];
    fputcsv($csv_file, $header);
    foreach ($results['#rows'] as $row) {
        $matches = $row['matches'];
        foreach($matches as $match) {
          $csv_row = [
            'id' => $row['id'],
            'entity_type' => $row['entity_type'],
            'title' => $row['title'],
            'content type' => $row['content type'],
            // 'node_url' => $row['node_url'],
            // 'edit_url' => $row['edit_url'],
            // 'lb_edit_url' => $row['lb_edit_url'],
            'status' => $row['status'],
            'author' => $row['author'],
            'created' => $row['created'],
            'updated' => $row['updated'],
            'field' => (string)$match,
          ];
          
          fputcsv($csv_file, $csv_row);
        }
        
      }
    
    fclose($csv_file);
    $response = new BinaryFileResponse($csv_file_path);
    $response->headers->set('Content-Type', 'text/csv');
    $response->setContentDisposition(
      ResponseHeaderBag::DISPOSITION_ATTACHMENT,
      $file_name
    );

    return $response;
  }

  /**
   * Resets the form back to the default state.
   */
  public function resetButton(array &$form, FormStateInterface $form_state): void {
    $form_state->setValue('needle', '');
    $form_state->set('results', FALSE);
    $form_state->set('results_table', FALSE);
    $form_state->set('results_markup', '');

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getTriggeringElement()['#name'] === 'search' && strlen($form_state->getValue('needle')) < 3) {
      $form_state->setErrorByName('needle', $this->t('The search target must contain 3 or more characters.'));
    }
  }

  /**
   * Helper function to form our table array.
   *
   * @param array $results
   *   The search form results to prepare.
   * @param ?string $status_filter
   *   Whether the status filter should be included in the results table.
   * @param ?string $bundle_filter
   *   Strip all results that don't match the node bundle filter.
   *
   * @return array
   *   The the filtered results table array.
   */
  public function filterResultsTable(array $results, ?string $status_filter = NULL, ?string $bundle_filter = NULL): array {
    if (empty($results)) {
      return [];
    }
    // Rearrange and clear out the excess to make printing easier.
    // Starting out, our results are separated by entity type.
    foreach ($results as $type => $typed_results) {
      // This first key will be the entity id.
      foreach (array_keys($typed_results) as $key) {
        // The secondary key will be a simple delta.
        foreach (array_keys($typed_results[$key]) as $secondary_key) {
          // Created a modified key to help us avoid collisions
          // while rearranging our results.
          $mod_key = implode('-', [$type, $key]);
          $results[$mod_key][] = $results[$type][$key][$secondary_key]->value;
        }
      }
      unset($results[$type]);
    }
    $data_rows = [];
    $node_manager = $this->entityTypeManager
      ->getStorage('node');
    foreach ($results as $mod_key => $matches) {
      $exploded = explode('-', $mod_key);
      [$type, $id] = $exploded;
      switch ($type) {
        case 'block_content':
        case 'paragraph':
        case 'node':
          $node = $node_manager->load($id);
          $node_type_machine_name = $node->getType();

          // if the bundle filter is set, check if the node type matches the filter. Proceed if match, skip if not.
          if ($bundle_filter && $bundle_filter !== 'All' && $node_type_machine_name !== $bundle_filter) {
            break;
          }

          // Check if workflow is being used.
          // If it is, use the current workflow status.
          // If it is not, use the published status.
          if ($node->hasField('moderation_state')) {
            if ($node->get('moderation_state')->value) {
              $status = $node->get('moderation_state')->value;
            }
          }
          else {
            $status = $node->isPublished() ? 'Published' : 'Not Published';
          }
          $node_url = Url::fromRoute('entity.node.canonical', ['node' => $id]);
          $node_url = $node_url->toString();
          $edit_url = Url::fromRoute('entity.node.edit_form', ['node' => $id]);
          $edit_url = $edit_url->toString();


          // If the status filter is set to all
          // or the status matches the filter,
          // define the entity value otherwise skip to the next entity.
          if ($status_filter && ($status_filter === 'All' || $status === $status_filter)) {
            // Check if we have an overridden layout or not.
            // If we didn't successfully load a node, go ahead
            // and treat it as a non-overridden node so that
            // the user will still see it in the results as a failsafe.
            $has_lb = $node && $node->hasField('layout_builder__layout');
            if ($has_lb) {
                $lb_edit_url = '/node/' . $id . '/layout';
            }
            $title = $node->getTitle();
            $node_type = node_get_type_label($node);
            $status = $status;
            $author = $node->getOwner()->getDisplayName();
            $created = date("m-d-Y", $node->getCreatedTime());
            $updated = date("m-d-Y", $node->changed->value);
          }

          break;

        case 'menu_link_content':
          if ($bundle_filter && $bundle_filter !== 'All') {
            break;
          }
          $menu_link_manager = $this->entityTypeManager->getStorage('menu_link_content');
          $menu_link = $menu_link_manager->load($id);
          $title = $menu_link->getTitle();

          break;

        case 'taxonomy_term':
          if ($bundle_filter && $bundle_filter !== 'All') {
            break;
          }
            $title = Term::load($id)->get('name')->value;
          break;

        default:
          $title = FALSE;
      }
      if (!$title) { 
        continue;
      }

      // Add all field matches to the last element of the rows array. We will split them in the render function or csv generation.
      $data_rows[] = [
        'id' => $id,
        'entity_type' => $type ?? '',
        'title' => $title ?? '',
        'content type' => $node_type ?? '',
        'node_url' => $node_url ?? '',
        'edit_url' => $edit_url ?? '',
        'lb_edit_url' => $lb_edit_url ?? '',
        'status' => $status ?? '',
        'author' => $author ?? '',
        'created' => $created ?? '',
        'updated' => $updated ?? '',
        'matches' => $matches,
      ];
      
    }


    $data_table = [
      '#type' => 'table',
      '#header' => [
        'id' => 'Entity ID',
        'entity_type' => 'Entity Type',
        'title' => 'Title',
        'type' => 'Type',
        'node_url' => 'Node URL',
        'edit_url' => 'Edit URL',
        'lb_edit_url' => 'Layout Builder URL',
        'status' => 'Status',
        'author' => 'Author',
        'created' => 'Created',
        'updated' => 'Updated',
        'matches' => 'Array of Matches',
      ],
      '#rows' => $data_rows,
      '#attributes' => NULL,
    ];

    return $data_table;
  }

  /**
   * Helper function to render our table array.
   *
   * @param array $results
   *   The search form results to prepare.
   *
   * @return array
   *   The renderable table array.
   */
  public function renderResultsTable($results): array {
    $render_rows = [];


    foreach ($results['#rows'] as $result) {
      switch ($result['entity_type']) {
        case 'block_content':
        case 'paragraph':
        case 'node':
          if($result['lbEditUrl']) {
            $entity_value = new FormattableMarkup('<a href="@nodeUrl">@title</a><br /><strong>Node:</strong> @nid (<a href="@editUrl">edit</a>) (<a href="/node/@nid/layout">layout</a>)<br /><strong>Type:</strong> @type<br /><strong>Status:</strong> @status<br /><strong>Author:</strong> @author<br /><strong>Created Date:</strong> @date<br /><strong>Last Updated:</strong> @update', [
              '@nid' => $result['id'],
              '@nodeUrl' => $result['nodeUrl'],
              '@editUrl' => $result['editUrl'],
              '@lbEditUrl' => $result['lbEditUrl'],
              '@title' => $result['title'],
              '@type' => $result['content_type'],
              '@status' => $result['status'],
              '@author' => $result['author'],
              '@date' => $result['created'],
              '@update' => $result['updated'],
            ]);
          }
          else {
            $entity_value = new FormattableMarkup('<a href="@nodeUrl">@title</a><br /><strong>Node:</strong> @nid (<a href="@editUrl">edit</a>)<br /><strong>Type:</strong> @type<br /><strong>Status:</strong> @status<br /><strong>Author:</strong> @author<br /><strong>Created Date:</strong> @date<br /><strong>Last Updated:</strong> @update', [
              '@nid' => $result['id'],
              '@nodeUrl' => $result['nodeUrl'],
              '@editUrl' => $result['editUrl'],
              '@title' => $result['title'],
              '@type' => $result['type'],
              '@status' => $result['status'],
              '@author' => $result['author'],
              '@date' => $result['created'],
              '@update' => $result['updated'],
            ]);
          }
          break;

        case 'menu_link_content':
          $entity_value = new FormattableMarkup('@title<br /><strong>Menu:</strong> @mid (<a href="/admin/structure/menu/item/@mid/edit?destination=/admin/find-text">edit</a>)', [
            '@mid' => $result['id'],
            '@title' => $result['title'],
          ]);
          break;
        case 'taxonomy_term':
          $entity_value = new FormattableMarkup('<a href="/taxonomy/term/@tid/">@title</a></br /><strong>Term:</strong> @tid (<a href="/taxonomy/term/@tid/edit?destination=/admin/find-text">edit</a>)', [
            '@tid' => $result['id'],
            '@title' => $result['title'],
          ]);
          break;
      }

      $matches = $result['matches'];
      $render_rows[] = [
        'id' => [
          'data' => $entity_value,
          'rowspan' => count($matches),
        ],
        'field' => array_shift($matches),
      ];
      // $count = count($matches);
      foreach ($matches as $match) {
        $render_rows[] = [
          // 'id' => [
          //   'data' => $entity_value,
          //   // Stretch the node row to cover all its matches.
          //   // 'rowspan' => ($count-1),
          // ],
          // Shift the first element out of the ray to match
          // to the node row.
          'field' => $match,
        ];
      }

    }

    $render_rows[] = [
      'id' => [
        'data' => 'test',
      ],
      'field' => 'test',
    ];
      
    $renderable_table = [
      '#type' => 'table',
      '#header' => [
        'id' => 'Entity ID',
        'field' => 'Field: Contents',
      ],
      '#rows' => $render_rows,
      '#attributes' => NULL,
    ];

    return $renderable_table;
  }

}
