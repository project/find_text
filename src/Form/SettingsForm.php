<?php

namespace Drupal\find_text\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Find Text settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'find_text.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'find_text_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $field_types = $config->get('field_types');
    $options = [];
    $checked = [];
    foreach ($field_types as $field_type => $field_type_settings) {
      $options[$field_type] = $field_type;
      if ($field_type_settings['allowed'] == 1) {
        $checked[] = $field_type;
      }
    }

    $form['field_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed field types to search'),
      '#options' => $options,
      '#default_value' => $checked,
    ];

    // @todo Add settings options to allow or disallow certain entities
    //   as well as specific bundles/types within entities.
    $form['allow_all_entities'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow all entities'),
      '#description' => $this->t('Allow Find Text to search allowed text fields regardless of which entities they are attached.'),
      '#default_value' => $config->get('allow_all_entities'),
    ];

    $form['save_as_csv'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save search results as CSV'),
      '#description' => $this->t('Save search results as a CSV file.'),
      '#default_value' => $config->get('save_as_csv'),
    ];

    // Field to enable/disable search results caching.
    $form['enable_search_results_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable search results cache'),
      '#description' => $this->t('If checked, the search results data is cached based on the entered search text.'),
      '#default_value' => $config->get('enable_search_results_cache'),
    ];

    // Field to provide cache duration.
    $form['search_results_cache_duration'] = [
      '#type' => 'number',
      '#title' => $this->t('Cache duration'),
      '#description' => $this->t('Enter time (in seconds) to provide cache expiry time for the search results data.'),
      '#default_value' => $config->get('search_results_cache_duration'),
      // Visible if search results cache is enabled.
      '#states' => [
        'visible' => [
          ':input[name="enable_search_results_cache"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $config = $this->configFactory->getEditable(static::SETTINGS);
    foreach ($form_state->getValue('field_types') as $field_type => $value) {
      $value = ($value === 0) ? 0 : 1;
      $config->set('field_types.' . $field_type . '.allowed', $value);
    }
    $config->set('allow_all_entities', $form_state->getValue('allow_all_entities'));
    $config->set('save_as_csv', $form_state->getValue('save_as_csv'));
    $config->set('enable_search_results_cache', $form_state->getValue('enable_search_results_cache'));
    $config->set('search_results_cache_duration', $form_state->getValue('search_results_cache_duration'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
