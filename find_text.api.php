/**
 * @file
 * Callbacks and hooks related to form system.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the search results set.
 *
 * @param array $results
 *   An array of the retreived search results.
 */
function hook_find_text_results(array &$results) {
  // Exclude a set of nodes to be ignored based on configuration.
  $exclude = \Drupal::config('mysite.find_text_ignore');
  foreach ($exclude as $exclude_id) {
    unset($results['node'][$exclude_id])
  }
}

/**
 * @} End of "addtogroup hooks".
 */
