# Find Text

Find Text is an editor/administrator tool to directly search all text fields on a site for a given string or regexp pattern. This allows for direct, and directed, database searches for text content without the need to use more advanced tools or requiring direct access to the database.

For instance, if a product name is going to be updated, Find Text can be used to find all instances of this name within text fields, including node body fields, text fields within paragraphs or blocks, or text values within custom fields. Similarly, instances of a specific HTML class, relative or absolute link, drupal entity, etc., can be searched for directly prior to any rendering or other processing occurs on the frontend.

The results table of the search includes the entities on which the text appears (e.g. node or menu), the specific field or fields in which the text is used, and the full content which contains the searched text. The matching text will be highlighted within the full text.

## Text Search Wildcards
Two text wildcard options are available as part of the text search.

Underscores (_) can be used as single-match wildcards. Including these within a text search will match any one character for each underscore used.

Percent signs (%) can be used as multi-match wildcards. Including these within a text search will match zero, one, or multiple characters wherever it is used.

## Render Markup
When using the render markup option, text matches in the results will be rendered when displayed. Content within tables, lists, or headings will be displayed as such for easier readability, though this may hide some text matches.

Searching for "table", for instance, will match any HTML tables within text content, though the matching tags will be hidden by the rendering process.

## Regexp
If the regexp option is checked, the search text will be treated as a regular expression rather than a specific text search.

More information on regular expressions may be found at https://www.regular-expressions.info.